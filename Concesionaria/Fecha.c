#include <stdio.h>
#include <string.h>
#include "Fecha.h"

/**
* Funcion que permite crear una nueva fecha.
* @fecha 24 06 2015
* @param dia Numero entero que representa el dia
* @param mes Numero entero que representa el mes
* @param anio Numero entero que representa el anio
* @return La fecha(dia,mes,anio)
*/
Fecha *fechaNew(int dia, int mes, int anio){
	Fecha *f = malloc(sizeof(Fecha));
	f->dia = dia;
	f->mes = mes;
	f->anio = anio;
	return f;
}


//MIRA SI EL ANO ES BISIESTO
int bisiesto(int anio)
{
	int esbisiesto = 0;
	if (anio % 400 == 0)
		esbisiesto = 1;
	else if (anio % 4 == 0 && anio % 100 != 0)
		esbisiesto = 1;
	return esbisiesto;
}

//MIRA SI LAS FECHAS SON CORRECTAS
int comprueba(Fecha *f){//0 no es correcte, 1 si es correcte
	int correcto = 0;

	if (f->anio >= 100 && f->anio < 200){
		correcto = 1;						
	}

	if (f->mes > 12 || f->mes < 0){
		correcto = 0;
	}

	if (f->mes == 1 && f->dia == 29){
		if (bisiesto(f->anio + 1900) == 0){
			correcto = 0;
		}
		else{ correcto = 1; }
	}
	return correcto;
}