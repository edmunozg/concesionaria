#ifndef VEHICULO_H
#define	VEHICULO_H
#include "../TdaLib/list.h"

typedef enum TipoVehiculo{
	Taxi = 1, GamaAlta = 2, GamaMedia = 3, GamaBaja = 4
}TipoVehiculo;

typedef struct TVehiculo{
	char Placa[10], Marca[50], Modelo[50];
	int AnioFabricacion, Concesionaria, Kilometraje;
	enum TipoVehiculo tipoVehiculo;
	char idCliente[13];
	List *mantenimientos;
}Vehiculo;

Vehiculo *vehiculoNew(char *Placa, char *Marca, char *Modelo,int Concesionaria, char *Kilometraje, int AnioFabricacion, TipoVehiculo tipoVehiculo,char *idCliente);
void ingresarVehiculo(List *vehiculos, List *clientes);
void guardarVehiculo(FILE *nombreArchivo, Vehiculo *v);
Vehiculo* readLineVehiculo(FILE *nombreArchivo);
void vehiculoPrint(Vehiculo *v);
NodeList* vehiculoSearchById(List *L, char* placa);
void vehiculoEliminar(List *vehiculos);
void vehiculoModificar(List *clientes);
TipoVehiculo elegirTipoVehiculo(int tipo);



#endif