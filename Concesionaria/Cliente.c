#include <stdio.h>
#include <string.h>
#include "Cliente.h"
#include "Vehiculo.h"

/**
* Función que permite crear un nuevo cliente.
* @author Esteban Muñoz Guevara
* @param nombre Cadena de texto que representa el nombre del cliente
* @param apellido Cadena de texto que representa el apellido del cliente
* @param identificacion Cadena de texto que representa la identificacion del cliente
* @fecha 24 de Junio 2015
*/
Cliente *clienteNew(char *nombre, char *apellido, char *identificacion){
	Cliente *c = malloc(sizeof(Cliente));
	strcpy(c->Nombre, nombre);
	strcpy(c->Apellido, apellido);
	strcpy(c->Identificacion, identificacion);
	c->vehiculos = listNew();
	return c;
}

/**
* Funcion que permite ingresar un nuevo cliente
* @fecha 26 de Junio 2015
* @fechaActulización 28 de Junio 2015
*/
void ingresarCliente(List *clientes){
	//clientes = listNew();//Creamos una nueva lista para los clientes
	NodeList *nodeCliente = NULL;
	Cliente *cliente = NULL;
	char nombre[50], apellido[50], identificacion[13];
	printf("INGRESE EL NOMBRE : ");//Pedimos al usuario por pantalla
	fflush(stdin); //Permite limpiar el buffer del teclado.
	scanf("%[^\n]s", &nombre); //Almacenamos la cadena. [^\n] Lee la cadena hasta encontrar un salto de linea
	printf("\nINGRESE EL APELLIDO : ");
	fflush(stdin);
	scanf("%[^\n]s", &apellido);
	printf("\nINGRESE IDENTIFICACION : ");
	fflush(stdin);
	scanf("%[^\n]s", &identificacion);
	cliente = clienteNew(nombre, apellido, identificacion);//Creamos un nuevo cliente
	nodeCliente = nodeListNew(cliente);//Creamos un nodo que contenga al cliente
	listAddNode(clientes, nodeCliente);//Agregamos el nodo a la lista
	//guardarCliente("Clientes.txt", cliente);//Persistencia del cliente
}

/**
* Funcion que permite escribir en un archivo el cliente.
* @fecha 28 de Junio 2015
* @param nombreArchivo Archivo donde se va a escribir
* @param c Cliente que se va a escribir en el archivo
* @return -1 Si hubo un errar al intentar abrir el archivo
*/
void guardarCliente(FILE *nombreArchivo, Cliente *c){
	FILE *archivo = NULL;
	archivo = fopen(nombreArchivo, "a");//Abrimos el archivo en modo escritura
	if (archivo == NULL)//Si hubo un error retornamos -1
		return -1;
	fprintf(archivo, "\n%s;%s;%s", c->Nombre, c->Apellido, c->Identificacion);//Escribimos en el archivo
	fclose(archivo);//Cerramos el archivo
}

/**
* Función que lee de un archivo una lista de clientes.
* @fecha 04 de Julio 2015
* @param nombreArchivo Archivo donde se va a leer
*/
Cliente* readLineCliente(FILE *nombreArchivo){
	Cliente* c = NULL;
	char lectura[200];
	char *ptr;
	char *nombre, *apellido, *identificacion;
	fscanf(nombreArchivo, " %[^\n]s", lectura);//Leemos el archivo lenea por lenea
	ptr = strtok(lectura, ";");
	while (ptr != NULL)//Leemos el string separado por ";"
	{
		nombre = ptr;
		apellido = strtok(NULL, ";");
		identificacion = strtok(NULL, ";");
		ptr = strtok(NULL, ";");
	}
	c = clienteNew(nombre, apellido, identificacion);
	return c;
}

/**
* Función que imprime el cliente.
* @fecha 05 de Julio 2015
* @param c Cliente a ser impriso
*/
void clientePrint(Cliente *c){
	printf("Nombre : %s \n", c->Nombre);
	printf("Apellido : %s \n", c->Apellido);
	printf("Identificacion : %s \n", c->Identificacion);
}

void clienteModificar(List *clientes){
	char identificacion[13];
	printf("INGRESE LA IDENTIFICACION DEL CLIENTE A MODIFICAR :");
	fflush(stdin);
	scanf("%[^\n]s", &identificacion);
	NodeList *nlCliente = clienteSearchById(clientes, identificacion);
	if (nlCliente != NULL){
		Cliente *cliente = nodeListGetCont(nlCliente);
		printf("\n----- DATOS ACTUALES DEL CLIENTE ----- \n");
		clientePrint(cliente);
		listRemoveNode(clientes, nlCliente);
		int t = listGetSize(clientes);
		printf("Tamanio: %d \n", t);
		printf("\n----- INGRESE LOS NUEVOS DATOS ----- \n");
		ingresarCliente(clientes);
	}
	else
		printf("CLIENTE NO EXISTE!");
}

void clienteEliminar(List *clientes){
	char identificacion[13];
	printf("\nINGRESE LA IDENTIFICACION DEL CLIENTE QUE QUIERE ELIMINAR :");
	fflush(stdin);
	scanf("%[^\n]s", &identificacion);
	NodeList *nlCliente = clienteSearchById(clientes, identificacion);
	int tama = listGetSize(clientes);
	printf("Tamanio antes: %d \n", tama);
	listRemoveNode(clientes, nlCliente);
	int tam = listGetSize(clientes);
	printf("Tamanio despues: %d \n", tam);
	printf("EL CLIENTE CON IDENTIFICACION : %s HA SIDO ELIMINADO \n", identificacion);
}

NodeList* clienteSearchById(List *L, char* identificacion){
	NodeList *iterator;
	for (iterator = L->header; iterator != NULL; iterator = iterator->next){
		Cliente *c = iterator->cont;
		if (strcmp(c->Identificacion, identificacion) == 0)
			return iterator;
	}
	return NULL;
}


/*
* Fuente : http://www.tutosytips.com/las-tablas-y-sus-estilos-en-twitter-bootstrap-3/
*/
void clientePrintBrowser(List *clientes){
	Cliente *c = NULL;
	Vehiculo *v = NULL;
	NodeList *iteratorc;
	NodeList *iteratorv;

	FILE *archivo = NULL;
	archivo = fopen("listadoVehículosDeUnCliente.html", "w");//w: se abre para escritura, se crea un fichero nuevo o se sobreescribe si ya existe
	if (archivo == NULL)//Si hubo un error retornamos -1
		return -1;
	//fprintf(archivo, "\n%s;%s;%s", c->Nombre, c->Apellido, c->Identificacion);//Escribimos en el archivo
	fprintf(archivo, "%s", "<!DOCTYPE html>\n");		
	fprintf(archivo, "%s", "<html lang='es'>\n");
	fprintf(archivo, "%s", "\t<head>\n");
	fprintf(archivo, "%s", "\t\t<meta charset='UTF-8'>\n");
	fprintf(archivo, "%s", "\t\t<meta name='viewport' content='width = device - width; initial - scale = 1.0'> \n");
	fprintf(archivo, "%s", "\t\t<title>Concesionaria</title>\n");
	fprintf(archivo, "%s", "\t\t<!-- Estilos CSS vinculados -->\n");
	fprintf(archivo, "%s", "\t\t<link href='css/bootstrap.min.css' rel='stylesheet'>\n");
	fprintf(archivo, "%s", "\t\t<link href='css/estilos.css' rel='stylesheet'>\n");
	fprintf(archivo, "%s", "\t<head>\n");
	fprintf(archivo, "%s", "\t<body>\n");
	fprintf(archivo, "%s", "\t\t<div class='container'>\n");
	fprintf(archivo, "%s", "\t\t\t<div class='page-header'>\n");
	fprintf(archivo, "%s", "\t\t\t\t<h1>Listado de vehículos de un cliente</h1>\n");
	fprintf(archivo, "%s", "\t\t\t</div>\n");
	fprintf(archivo, "%s", "\t\t\t<table class='table table - bordered'>\n");
	fprintf(archivo, "%s", "\t\t\t\t<thead>\n");
	fprintf(archivo, "%s", "\t\t\t\t<tr class='active'>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Cliente</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Marca</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Modelo</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Placa</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Año</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Kilometraje(Km)</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Tipo</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t</tr>\n");
	fprintf(archivo, "%s", "\t\t\t\t</thead>\n");
	fprintf(archivo, "%s", "\t\t\t\t<tbody>\n");
	
	for (iteratorc = clientes->header; iteratorc != NULL; iteratorc = iteratorc->next){
		c = nodeListGetCont(iteratorc);
		if (!listIsEmpty(c->vehiculos)){
			for (iteratorv = c->vehiculos->header; iteratorv != NULL; iteratorv = iteratorv->next){
				v = nodeListGetCont(iteratorv);
				fprintf(archivo, "%s", "\t\t\t\t\t<tr class='info'>\n");
				fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->idCliente);
				fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->Marca);
				fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->Modelo);
				fprintf(archivo,  "\t\t\t\t\t\t<td>%s</td>\n", v->Placa);
				fprintf(archivo, "\t\t\t\t\t\t<td>%d</td>\n", v->AnioFabricacion);
				fprintf(archivo, "\t\t\t\t\t\t<td>%d</td>\n", v->Kilometraje);
				fprintf(archivo, "\t\t\t\t\t\t<td>en proceso</td>\n", v->tipoVehiculo);
				fprintf(archivo, "%s", "\t\t\t\t\t</tr>\n");
			}
		}
	}

	fprintf(archivo, "%s", "\t\t\t\t</tbody>\n");
	fprintf(archivo, "%s", "\t\t\t</table>\n");
	fprintf(archivo, "%s", "\t\t</div>\n");
	fprintf(archivo, "%s", "\t\t<!-- Js vinculados -->\n");
	fprintf(archivo, "%s", "\t\t<script src='http://code.jquery.com/jquery-latest.min.js'></script>\n");
	fprintf(archivo, "%s", "\t\t<script src='js/responsive.js'></script>\n");
	fprintf(archivo, "%s", "\t\t<script src='js/bootstrap.min.js'></script>\n");
	fprintf(archivo, "%s", "\t</body>\n");
	fprintf(archivo, "%s", "</html>\n");



	fclose(archivo);//Cerramos el archivo
	system("start miArchivo.html");
}

void clientePrintBrowserById(List *clientes,char *identificacion){
	Cliente *c = NULL;
	Vehiculo *v = NULL;
	NodeList *iteratorv;

	FILE *archivo = NULL;
	archivo = fopen("listadoVehículosDeUnCliente.html", "w");//w: se abre para escritura, se crea un fichero nuevo o se sobreescribe si ya existe
	if (archivo == NULL)//Si hubo un error retornamos -1
		return -1;
	//fprintf(archivo, "\n%s;%s;%s", c->Nombre, c->Apellido, c->Identificacion);//Escribimos en el archivo
	fprintf(archivo, "%s", "<!DOCTYPE html>\n");
	fprintf(archivo, "%s", "<html lang='es'>\n");
	fprintf(archivo, "%s", "\t<head>\n");
	fprintf(archivo, "%s", "\t\t<meta charset='UTF-8'>\n");
	fprintf(archivo, "%s", "\t\t<meta name='viewport' content='width = device - width; initial - scale = 1.0'> \n");
	fprintf(archivo, "%s", "\t\t<title>Concesionaria</title>\n");
	fprintf(archivo, "%s", "\t\t<!-- Estilos CSS vinculados -->\n");
	fprintf(archivo, "%s", "\t\t<link href='css/bootstrap.min.css' rel='stylesheet'>\n");
	fprintf(archivo, "%s", "\t\t<link href='css/estilos.css' rel='stylesheet'>\n");
	fprintf(archivo, "%s", "\t<head>\n");
	fprintf(archivo, "%s", "\t<body>\n");
	fprintf(archivo, "%s", "\t\t<div class='container'>\n");
	fprintf(archivo, "%s", "\t\t\t<div class='page-header'>\n");
	fprintf(archivo, "%s", "\t\t\t\t<h1>Listado de vehículos de un cliente</h1>\n");
	fprintf(archivo, "%s", "\t\t\t</div>\n");
	fprintf(archivo, "%s", "\t\t\t<table class='table table - bordered'>\n");
	fprintf(archivo, "%s", "\t\t\t\t<thead>\n");
	fprintf(archivo, "%s", "\t\t\t\t<tr class='active'>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Cliente</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Marca</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Modelo</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Placa</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Año</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Kilometraje(Km)</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t\t<th>Tipo</th>\n");
	fprintf(archivo, "%s", "\t\t\t\t</tr>\n");
	fprintf(archivo, "%s", "\t\t\t\t</thead>\n");
	fprintf(archivo, "%s", "\t\t\t\t<tbody>\n");

	NodeList *nlCliente = clienteSearchById(clientes, identificacion);
	c = nodeListGetCont(nlCliente);
	if (!listIsEmpty(c->vehiculos)){
		for (iteratorv = c->vehiculos->header; iteratorv != NULL; iteratorv = iteratorv->next){
			v = nodeListGetCont(iteratorv);
			fprintf(archivo, "%s", "\t\t\t\t\t<tr class='info'>\n");
			fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->idCliente);
			fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->Marca);
			fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->Modelo);
			fprintf(archivo, "\t\t\t\t\t\t<td>%s</td>\n", v->Placa);
			fprintf(archivo, "\t\t\t\t\t\t<td>%d</td>\n", v->AnioFabricacion);
			fprintf(archivo, "\t\t\t\t\t\t<td>%d</td>\n", v->Kilometraje);
			fprintf(archivo, "\t\t\t\t\t\t<td>en proceso</td>\n", v->tipoVehiculo);
			fprintf(archivo, "%s", "\t\t\t\t\t</tr>\n");
		}
	}

	fprintf(archivo, "%s", "\t\t\t\t</tbody>\n");
	fprintf(archivo, "%s", "\t\t\t</table>\n");
	fprintf(archivo, "%s", "\t\t</div>\n");
	fprintf(archivo, "%s", "\t\t<!-- Js vinculados -->\n");
	fprintf(archivo, "%s", "\t\t<script src='http://code.jquery.com/jquery-latest.min.js'></script>\n");
	fprintf(archivo, "%s", "\t\t<script src='js/responsive.js'></script>\n");
	fprintf(archivo, "%s", "\t\t<script src='js/bootstrap.min.js'></script>\n");
	fprintf(archivo, "%s", "\t</body>\n");
	fprintf(archivo, "%s", "</html>\n");



	fclose(archivo);//Cerramos el archivo
	system("start listadoVehículosDeUnCliente.html");
}



