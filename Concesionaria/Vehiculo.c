#include <stdio.h>
#include <string.h>
#include "Vehiculo.h"
#include "Cliente.h"
#include "Mantenimiento.h"


/**
* Funci�n que permite saber el tipo de vehiculo
* @tipo Entero que representa el tipo de vehiculo
*/
TipoVehiculo elegirTipoVehiculo(int tipo){
	TipoVehiculo tv;
	switch (tipo)
	{
	case 1:
	{
		tv = Taxi;
		return tv;
	}
	case 2:
	{
		tv = GamaAlta;
		return tv;
	}
	case 3:
	{
		tv = GamaMedia;
		return tv;
	}
	case 4:
	{
		tv = GamaBaja;
		return tv;
	}
	default:
		break;
	}
}

/**
* Funci�n que permite crear un nuevo vehiculo.
* @fecha 24 de Junio 2015
*/
Vehiculo *vehiculoNew(char *Placa, char *Marca, char *Modelo,int Concesionaria, int Kilometraje, int AnioFabricacion, TipoVehiculo tipoVehiculo,char *idCliente){
	Vehiculo *v = malloc(sizeof(Vehiculo));
	strcpy(v->Placa, Placa);
	strcpy(v->Marca, Marca);
	strcpy(v->Modelo, Modelo);
	v->Concesionaria, Concesionaria;
	v->Kilometraje, Kilometraje;
	v->AnioFabricacion = AnioFabricacion;
	v->tipoVehiculo = tipoVehiculo;
	strcpy(v->idCliente , idCliente);
	v->mantenimientos = listNew();
	return v;
}

/**
* Funci�n que permite ingresar un nuevo vehiculo
* @fechaCreaci�n 26 de Junio 2015
* @fechaActulizaci�n 28 de Junio 2015
*/
void ingresarVehiculo(List *vehiculos,List *clientes){
	char placa[10], marca[50], modelo[50];
	int anioFabricacion, concesionaria, kilometraje;
	int tipoVehiculo;
	TipoVehiculo tv;
	char idCliente[12];
	NodeList *nodeVehiculo = NULL;
	Vehiculo *vehiculo = NULL;
	printf("\nINGRESE EL NUMERO DE PLACA : ");
	fflush(stdin);
	scanf("%[^\n]s", &placa);
	printf("\nINGRESE LA MARCA : ");
	fflush(stdin); //Permite limpiar el buffer del teclado.
	scanf("%[^\n]s", &marca); //Almacenamos la cadena. [^\n] Lee la cadena hasta encontrar un salto de linea
	printf("\nINGRESE EL MODELO : ");
	fflush(stdin);
	scanf("%[^\n]s", &modelo);
	printf("\nCOMPRADO EN LA CONCESIONARIA");
	printf("\nSI->1 NO->0: ");
	fflush(stdin);
	scanf("%d", &concesionaria);
	if (concesionaria){
		kilometraje = 0;		
	}
	else{
		printf("\nINGRESE EL KILOMETRAJE ACTUAL: ");
		fflush(stdin);
		scanf("%d", &kilometraje);
	}
	printf("\nINGRESE EL ANIO DE FABRICACION : ");
	fflush(stdin);
	scanf("%d", &anioFabricacion);
	printf("\nTIPO DE VEHICULO  ");
	printf("\n 1. - TAXI ");
	printf("\n 2. - GAMA ALTA ");
	printf("\n 3. - GAMA MEDIA ");
	printf("\n 4. - GAMA BAJA ");
	printf("\nELIJA EL TIPO DE VEHICULO : ");
	fflush(stdin);
	scanf("%d", &tipoVehiculo);
	printf("\nINGRESE IDENTIFICACION DEL CLIENTE(DUENIO DEL VEHICULO) : ");
	fflush(stdin);
	scanf("%[^\n]s", &idCliente);
	tv = elegirTipoVehiculo(tipoVehiculo);
	vehiculo = vehiculoNew(placa, marca, modelo,concesionaria, kilometraje, anioFabricacion, tipoVehiculo, elegirTipoVehiculo, tv,idCliente);
	//Si el vehiculo se compro en la concesionaria se generan los mantenimientos
	generarMantenimiento(vehiculo->mantenimientos, concesionaria, kilometraje);
	NodeList *nlCliente = clienteSearchById(clientes, idCliente);
	if (nlCliente != NULL){
		Cliente *cliente = nodeListGetCont(nlCliente);
		listAddNode(cliente->vehiculos, vehiculo);//Asocio el vehiculo con el due�o
	}
	else
		printf("CLIENTE NO EXISTE!");
	nodeVehiculo = nodeListNew(vehiculo);//Creamos un nodo que contenga al cliente
	listAddNode(vehiculos, nodeVehiculo);//Agregamos el nodo a la lista
	//guardarVehiculo("Vehiculos.txt", vehiculo);//Persistencia del cliente
	printf("\nINGRESADO EXITOSAMENTE");
}

/**
* Funci�n que permite escribir en un archivo el vehiculo.
* @fechaCreaci�n 28 de Junio 2015
* @param nombreArchivo Archivo donde se va a escribir
* @param v Vehiculo que se va a escribir en el archivo
* @return -1 Si hubo un errar al intentar abrir el archivo
*/
void guardarVehiculo(FILE *nombreArchivo, Vehiculo *v){
	FILE *archivo = NULL;
	archivo = fopen(nombreArchivo, "a");//Abrimos el archivo en modo escritur
	if (archivo == NULL)//Si hubo un error retornamos -1
		return -1;
	fprintf(archivo, "\n %s;%s;%s;%d;%d", v->Marca, v->Modelo, v->Kilometraje, v->AnioFabricacion, v->tipoVehiculo);//Escribimos en el archivo
	fclose(archivo);//Cerramos el archivo
}

/**
* Funci�n que lee de un archivo una lista de vechiculos.
* @fecha 03 de Julio 2015
* @param nombreArchivo Archivo donde se va a leer
*/
Vehiculo* readLineVehiculo(FILE *nombreArchivo){
	char lectura[200];
	char *ptr;
	char *placa, *marca, *modelo,*idCliente;
	int anioFabricacion, concesionaria, kilometraje;
	TipoVehiculo tv;
	Vehiculo* v = NULL;
	fscanf(nombreArchivo, " %[^\n]s", lectura);//Leemos el archivo lenea por lenea
	ptr = strtok(lectura, ";");
	while (ptr != NULL)
	{
		placa = ptr;
		marca = strtok(NULL, ";");
		modelo = strtok(NULL, ";");
		concesionaria = strtok(NULL, ";");
		kilometraje = strtok(NULL, ";");
		anioFabricacion = strtok(NULL, ";");
		tv = elegirTipoVehiculo(strtok(NULL, ";"));
		idCliente = strtok(NULL, ";");
		ptr = strtok(NULL, ";");
	}
	v = vehiculoNew(placa, marca, modelo,concesionaria, kilometraje, anioFabricacion, tv,idCliente);
	return v;
}

/**
* Funci�n que imprime el vehiculo.
* @fecha 05 de Julio 2015
* @param v Vehiculo a ser impriso
*/
void vehiculoPrint(Vehiculo *v){
	printf("Placa :%s \n", v->Placa);
	printf("Marca : %s \n", v->Marca);
	printf("Modelo : %s \n", v->Modelo);
	printf("Concesionaria : %d \n", v->Concesionaria);
	printf("Kilometraje : %d \n", v->Kilometraje);
	printf("Anio de fabricacion : %d \n", atoi(v->AnioFabricacion));
	printf("Tipo de vehiculo : %s \n", v->tipoVehiculo);
}

void vehiculoModificar(List *vehiculos,List *clientes){
	char placa[13];
	printf("INGRESE EL NUMERO DE PLACA DEL VEHICULO A MODIFICAR :");
	fflush(stdin);
	scanf("%[^\n]s", &placa);
	NodeList *nlVehiculo = vehiculoSearchById(vehiculos, placa);
	if (nlVehiculo != NULL){
		Vehiculo *vehiculo = nodeListGetCont(nlVehiculo);
		printf("\n----- DATOS ACTUALES DEL CLIENTE ----- \n");
		vehiculoPrint(vehiculo);
		listRemoveNode(vehiculos, nlVehiculo);
		int t = listGetSize(vehiculos);
		printf("Tamanio: %d \n", t);
		printf("\n----- INGRESE LOS NUEVOS DATOS ----- \n");
		ingresarVehiculo(vehiculos, clientes);
	}
	else
		printf("VEHICULO NO EXISTE!");
}

void vehiculoEliminar(List *vehiculos){
	char placa[10];
	printf("\nINGRESE EL NUMERO DE PLACA DEL VEHICULO QUE QUIERE ELIMINAR :");
	fflush(stdin);
	scanf("%[^\n]s", &placa);
	NodeList *nlVehiculo = vehiculoSearchById(vehiculos, placa);
	int tama = listGetSize(vehiculos);
	printf("Tamanio antes: %d \n", tama);
	listRemoveNode(vehiculos, nlVehiculo);
	int tam = listGetSize(vehiculos);
	printf("Tamanio despues: %d \n", tam);
	printf("EL VEHICULO CON NUMERO DE PLACA : %s HA SIDO ELIMINADO \n", placa);
}

NodeList* vehiculoSearchById(List *L, char* placa){
	NodeList *iterator;
	for (iterator = L->header; iterator != NULL; iterator = iterator->next){
		Vehiculo *v = iterator->cont;
		if (strcmp(v->Placa, placa) == 0)
			return iterator;
	}
	return NULL;
}