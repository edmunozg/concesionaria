#ifndef FECHA_H
#define	FECHA_H

typedef struct TFecha{
	int dia, mes, anio;
}Fecha;

Fecha *fechaNew(int dia, int mes, int anio);
int bisiesto(int year);
int comprueba(Fecha *f);

#endif
