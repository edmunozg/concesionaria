#include <stdio.h>
#include <string.h>
#include "Mantenimiento.h"
#include "Vehiculo.h"

Mantenimiento *mantenimientoNew(Fecha *fechaMantenimiento, float costo, double recorridoActual, double recorridoMaximo){
	Mantenimiento *m = malloc(sizeof(Mantenimiento));
	m->fechaMantenimiento = fechaMantenimiento;
	m->costo = costo;
	m->recorridoActual = recorridoActual;
	m->recorridoMaximo = recorridoMaximo;
	return m;
}

/*
*Se generan los mantenimientos del vehiculo
*/
void generarMantenimiento(List *mantenimientos, int concesionaria, double recorridoActual){
	Mantenimiento *m = NULL;
	NodeList *nodeList = NULL;
	float costo=0;
	Fecha *fechaMantenimiento;
	double recorridoMaximo=0;
	if (concesionaria){
		while (recorridoMaximo <= 100000)
		{
			costo = 200;
			recorridoMaximo += 3000;
			if (listGetSize(mantenimientos) % 4 == 0){//mantenimiento m�ltiplo de 4 cuesta $800
				costo = 800;
				m = mantenimientoNew(NULL, costo, recorridoActual, recorridoMaximo);
				nodeList = nodeListNew(m);
				listAddNode(mantenimientos, nodeList);
			}
			else{
				m = mantenimientoNew(NULL, costo, recorridoActual, recorridoMaximo);
				nodeList = nodeListNew(m);
				listAddNode(mantenimientos, nodeList);
			}
		}
	}
	else
		ingregarMantenimiento(mantenimientos, recorridoActual);
}

void ingregarMantenimiento(List *mantenimientos, double recorridoActual){
	NodeList *nodeMantenimiento = NULL;
	Mantenimiento *m = NULL;
	float costo = 200;
	Fecha *fechaMantenimiento;
	int dia, mes, anio;
	double recorridoMaximo = 0;
	printf("\nINGRESE LA FECHA DE MANTENIMIENTO (DD:MM:AA) EJEMPLO 15 06 2015 : ");
	fflush(stdin);
	scanf("%d %d %d", &dia, &mes, &anio);
	fechaMantenimiento = fechaNew(dia, mes, anio);
	recorridoMaximo = recorridoActual + 3000;
	m = mantenimientoNew(fechaMantenimiento, costo, recorridoActual, recorridoMaximo);
	nodeMantenimiento = nodeListNew(m);//Creamos un nodo que contenga el mantenimiento
	listAddNode(mantenimientos, nodeMantenimiento);//Agregamos el nodo a la lista
}