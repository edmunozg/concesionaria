//Librerias
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "Cliente.h"
#include "Vehiculo.h"
#include "../TdaLib/list.h"
#include "../TdaLib/queue.h"

//Tipo de datos
typedef Queue Concesionaria;

//Prototipos de funciones
void imprimirMenu();
void agregarVehiculoCliente(List *clientes, List *vehiculos);
void listadoDeVehiculosDeUnCliente(List *clientes);
//Funci�n principal
void main(){
	List *clientes = listNew();
	List *vehiculos = listNew();
	Concesionaria *c = queueNew();
	int opcion = 0;
	do
	{
		imprimirMenu();
		printf("\n\tINGRESE UNA OPCION :");
		scanf("%d", &opcion);
		switch (opcion)
		{
		case 1:
		{
			clientes = listReadFile("Clientes.txt", readLineCliente);
			vehiculos = listReadFile("Vehiculos.txt", readLineVehiculo);
			agregarVehiculoCliente(clientes, vehiculos);
			//clientePrintBrowser(clientes);
			//listPrint(clientes, clientePrint);
			//listPrint(vehiculos, vehiculoPrint);
			printf("\tEL AMBIENTE DESDE ARCHIVO SE HA GENERADO SATISFACTORIAMENTE \n");
			printf("\tPRESIONE ENTER PARA REGRESAR AL MENU PRINCIPAL\n");
			getch();
			system("cls");
			break;
		}
		case 2:
		{
			int opcionItem = 0;
			printf("\t A. INGRESAR  \n");
			printf("\t B. MODIFICAR \n");
			printf("\t C. ELIMINAR  \n");
			printf("\nINGRESE UNA OPCION:");
			scanf("%d", &opcionItem);
			switch (opcionItem)
			{
			case 1:
				ingresarCliente(clientes);
				printf("PRESIONE ENTER PARA REGRESAR AL MENU");
				getch();
				system("cls");
				break;
			case 2:
				//clientes = listReadFile("Clientes.txt", readLineCliente);
				clienteModificar(clientes);
				int s = listGetSize(clientes);
				printf("TAMANIO DESPUES: %d", s);
				listPrint(clientes, clientePrint);
				printf("PRESIONE ENTER PARA REGRESAR AL MENU");
				getch();
				system("cls");
				break;
			case 3:
				//clientes = listReadFile("Clientes.txt", readLineCliente);
				clienteEliminar(clientes);
				printf("Clientes actuales\n");
				listPrint(clientes, clientePrint);
				break;
			default:
				break;
			}
			break;
		}
		case 3:
		{
			int opcionItem = 0;
			printf("\t A. INGRESAR  \n");
			printf("\t B. MODIFICAR \n");
			printf("\t C. ELIMINAR  \n");
			printf("\nINGRESE UNA OPCION:");
			scanf("%d", &opcionItem);
			switch (opcionItem)
			{
			case 1:
				ingresarVehiculo(vehiculos,clientes);
				printf("PRESIONE ENTER PARA REGRESAR AL MENU");
				int s1 = listGetSize(vehiculos);
				printf("\nTAMANIO DESPUES: %d", s1);
				getch();
				system("cls");
				break;
			case 2:
				//vehiculos = listReadFile("Vehiculos.txt", readLineVehiculo);
				vehiculoModificar(vehiculos);
				int s = listGetSize(vehiculos);
				printf("TAMANIO DESPUES: %d", s);
				listPrint(vehiculos, vehiculoPrint);
				printf("PRESIONE ENTER PARA REGRESAR AL MENU");
				getch();
				system("cls");
				break;
			case 3:
				//vehiculos = listReadFile("Vehiculos.txt", readLineVehiculo);
				vehiculoEliminar(vehiculos);
				printf("Vehiculos\n");
				listPrint(vehiculos, vehiculoPrint);
				break;
			default:
				break;
			}
			break;
		}
		case 4:
		{
			int opcionItem = 0;
			printf("\t A. LISTADO DE VEHICULOS ATENDIDOS EN EL DIA  \n");
			printf("\t B. LISTADO DE VEHICULOS ATENDIDOS EN EL SEMANA \n");
			printf("\t C. LISTADO DE VEHICULOS DE UN CLIENTE  \n");
			printf("\t D. MANTENIMIENTOS DE VEHICULOS  \n");
			printf("\t\t I.   LISTAR MANTENIMIENTOS  \n");
			printf("\t\t II.  LISTAR MANTENIMIENTOS PENDIENTES  \n");
			printf("\t\t III. LISTAR MANTENIMIENTOS REALIZADOSS  \n");
			printf("\nINGRESE UNA OPCION:");
			scanf("%d", &opcionItem);
			switch (opcionItem)
			{
				case 1:
					ingresarCliente(clientes);
					printf("PRESIONE ENTER PARA REGRESAR AL MENU");
					getch();
					system("cls");
					break;
				case 2:
					//clientes = listReadFile("Clientes.txt", readLineCliente);
					clienteModificar(clientes);
					int s = listGetSize(clientes);
					printf("TAMANIO DESPUES: %d", s);
					listPrint(clientes, clientePrint);
					printf("PRESIONE ENTER PARA REGRESAR AL MENU");
					getch();
					system("cls");
					break;
				case 3:
					listadoDeVehiculosDeUnCliente(clientes);
					break;
				case 4:
					//clientes = listReadFile("Clientes.txt", readLineCliente);
					clienteEliminar(clientes);
					printf("Clientes actuales\n");
					listPrint(clientes, clientePrint);
					break;					
				default:
					break;
			}
			break;
		}
		default:
			break;
		}
	} while (opcion != 5);
	system("exit");
}

//Definici�n de funciones
/**
* Funcion que imprime el men� de opciones.
* @author Esteban Mu�oz Guevara
* @fecha 23 de Junio 2015
*/
void imprimirMenu(){
	printf("\n\n\n\t\tESCUELA SUPERIOR POLITECNICA DEL LITORAL\n");
	printf("\tFACULTAD DE INGENIERIA EN ELECTRICIDAD Y COMPUTACION \n");
	printf("\t\t\tESTRUCTURA DE DATOS \n");
	printf("\t\t\t   CONCESIONARIA \n");
	printf("\t*****************************************************************\n");
	printf("\t*  1. GENERAR AMBIENTE DESDE ARCHIVO \t\t\t\t*\n");
	printf("\t*  2. CLIENTES \t\t\t\t\t\t\t*\n");
	printf("\t*  \t 1. INGRESAR \t\t\t\t\t\t*\n");
	printf("\t*  \t 2. MODIFICAR \t\t\t\t\t\t*\n");
	printf("\t*  \t 3. ELIMINAR \t\t\t\t\t\t*\n");
	printf("\t*  3. VEHICULO \t\t\t\t\t\t\t*\n");
	printf("\t*  \t 1. INGRESAR  \t\t\t\t\t\t*\n");
	printf("\t*  \t 2. MODIFICAR \t\t\t\t\t\t*\n");
	printf("\t*  \t 3. ELIMINAR \t\t\t\t\t\t*\n");
	printf("\t*  \t 4. TALLER \t\t\t\t\t\t*\n");
	printf("\t*  \t\t   I. INGRESAR \t\t\t\t\t*\n");
	printf("\t*  \t\t  II. ATENDER \t\t\t\t\t*\n");
	printf("\t*  \t\t III. FINALIZAR \t\t\t\t*\n");
	printf("\t*  4. GENERAR REPORTES \t\t\t\t\t\t*\n");
	printf("\t*  \t A. LISTADO DE VEHICULOS ATENDIDOS EN EL DIA \t\t*\n");
	printf("\t*  \t B. LISTADO DE VEHICULOS ATENDIDOS EN EL SEMANA \t*\n");
	printf("\t*  \t C. LISTADO DE VEHICULOS DE UN CLIENTE \t\t\t*\n");
	printf("\t*  \t D. MANTENIMIENTOS DE VEHICULOS \t\t\t*\n");
	printf("\t*  \t\t   I. LISTAR MANTENIMIENTOS \t\t\t*\n");
	printf("\t*  \t\t  II. LISTAR MANTENIMIENTOS PENDIENTES \t\t*\n");
	printf("\t*  \t\t III. LISTAR MANTENIMIENTOS REALIZADOS \t\t*\n");
	printf("\t*  5. SALIR \t\t\t\t\t\t\t*\n");
	printf("\t*\t\t\t\t\t\t\t\t*\n");
	printf("\t*****************************************************************\n");
}

void agregarVehiculoCliente(List *clientes, List *vehiculos){
	Cliente *c = NULL;
	Vehiculo *v = NULL;
	NodeList *iteratorc;
	NodeList *iteratorv;
	for (iteratorc = clientes->header; iteratorc != NULL; iteratorc = iteratorc->next){
		c = nodeListGetCont(iteratorc);
		if (!listIsEmpty(vehiculos)){
			for (iteratorv = vehiculos->header; iteratorv != NULL; iteratorv = iteratorv->next){
				v = nodeListGetCont(iteratorv);
				if (strcmp(c->Identificacion, v->idCliente) == 0)
					listAddNode(c->vehiculos, nodeListNew(v));
			}
		}
	}
}


void listadoDeVehiculosDeUnCliente(List *clientes) {
	char identificacion[12];
	printf("\nINGRESE LA IDENTIFICACION DEL CLIENTE : ");
	fflush(stdin);
	scanf("%[^\n]s", &identificacion);
	clientePrintBrowserById(clientes, identificacion);
}
