#ifndef CLIENTE_H
#define	CLIENTE_H

#include "../TdaLib/list.h"

typedef struct TCliente{
	char Nombre[50], Apellido[50], Identificacion[13];
	List *vehiculos;
}Cliente;

Cliente *clienteNew(char *Nombre, char *Apellido, char *Identificacion);
void ingresarCliente();
void guardarCliente(char *nameFile, Cliente *c);
void clientePrint(Cliente *c);
Cliente* readLineCliente(FILE *nombreArchivo);
void clienteModificar(List *clientes);
NodeList* clienteSearchById(List *L, Generic value);
void clienteEliminar(List *clientes);
void clientePrintBrowserById(List *clientes, char *identificacion);
void clientePrintBrowser(List *clientes);

#endif