#ifndef CLIENTE_H
#define	CLIENTE_H

#include "Fecha.h"
#include "../TdaLib/list.h"

typedef struct TMantenimiento {
	Fecha *fechaMantenimiento;//Fecha del mantenimiento
	float costo;//Valor del mantenimiento
	double recorridoActual;//Kilometraje al momento de hacer el mantenimiento
	double recorridoMaximo;//Kilometraje maximo para le proximo mantenimiento
}Mantenimiento;


Mantenimiento *mantenimientoNew(Fecha *fechaMantenimiento, float costo, double recorridoActual, double recorridoMaximo);
void generarMantenimiento(List *mantenimientos, int concesionaria, double recorridoActual);
void ingregarMantenimiento(List *mantenimientos, double recorridoActual);

#endif